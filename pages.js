
var pages = {}

function addPage(name,p) {
  p._NAME = name;
  if( p._NAME == "" ) p._NAME = "beginning";
  pages[window.location.pathname+name] = p;
}

function getPage( name ) {
    for( var i in pages ) {
        var page = pages[i];
        if( page._NAME == name )
        {
            return page;
        }
    }

   return null;
}

addPage("",{
  scenes:["assets/scene1.dae"],
  sky:"assets/house_sky.jpg",
  description: "On a lonely autumn night, you are woken up from your sleep by strange lights coming through the window of your farmhouse. You look outside, and see a strange meteor falling through the sky, landing on a nearby hill. You get dressed and go outside to investigate...",
  sounds: SceneMusicDTO.makeMusicConfig([1,2]),
  flags:["fog"],
  actions:[
    {
      trigger: "TRIGGER_Window",
      description: "Climb the hill",
      page:"climb_hill",
    },
    {
      trigger: "TRIGGER_Door",
      description: "Approach the meteor",
      page:"approach_the_meteor",
    },
    /* Example of an object with sound effect */
    {
      trigger: "TRIGGER_paper",
      showText: "Who knows the end? What has risen may sink, and what has sunk may rise. Loathsomeness waits and dreams in the deep, and decay spreads over the tottering cities of men.",
      sounds: [{
        type: "effect",
        loop: false,
        path: "assets/sounds/Paper SFX.wav",
        volume:.3
      }]
    },
    {
      trigger: "default_ncl1_1",
      showText: "JAM#2 - STORYTIME\nGame Jammers : http://jamming.games\narec, democore, ehasson, hsmith, richard, stefan, thegoatee, waking_the_witch",
      sounds: [{
        type: "effect",
        loop: false,
        path: "assets/sounds/Paper SFX.wav",
        volume:.3
      }]
    },
    {
      trigger: "TRIGGER_lamp",
      sounds: [{
        type: "effect",
        loop: false,
        path: "assets/sounds/Light On SFX.wav",
        volume:.3
      }]
    },
    {
      trigger: "TRIGGER_mug",
      sounds: [{
        type: "effect",
        loop: false,
        path: "assets/sounds/Drink Out of Mug SFX.wav",
        volume:.3
      }]
    },
    {
      trigger: "TRIGGER_radio",
      sounds: [{
        type: "effect",
        loop: false,
        path: "assets/sounds/Radio SFX 1.wav",
        volume:.3,
        name: "radio"
      }]
    },
    {
      trigger: "Comforter",
      showText: "I can't sleep now!"
    }
  ]
})

addPage("approach_the_meteor",{
  scenes:["assets/approach_the_meteor.dae"],
  textBoxOffset: { x:0, y:0.35, z: 0.25 },
  sky:"assets/house_sky.jpg",
  description: "While it only appeared to be a few hundred meters away, you feel like you’ve been walking for hours.  A mist slowly thickens around you until you realize you’re not sure which way you are headed or which way you have come.",
  actions:[
    {
      trigger: "TRIGGER_RUN",
      description: "Run forward",
      page: "run_forward",
    },
    {
      trigger: "TRIGGER_TURN_BACK",
      description: "Turn back",
      textBoxOffset: { x:0, y:0.4, z: -0.2 },
      page: "turn_back",
    },
    {
      trigger: "TRIGGER_YELL",
      description: "Yell out loudly into the mist",
      page: "yell_out",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([2,3]),
})

addPage("run_forward",{
  scenes:["assets/run_forward.dae"],
  textBoxOffset: { x:0, y:0.2, z: 0 },
  sky:"assets/house_sky.jpg",
  description: "In a panic you run in the direction you think was the right way, but stumble on some root or rock.  You find yourself face in the dirt, with the skin on your arms and knees scraped and bleeding.",
  actions:[
    {
      trigger: "TRIGGER_TURN_BACK",
      description: "Turn back",
      textBoxOffset: { x:0, y:0.4, z: -0.2 },
      page: "turn_back",
    },
    {
      trigger: "TRIGGER_YELL",
      description: "Yell out loudly into the mist",
      page: "yell_out",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([7,8]),
})

addPage("yell_out",{
  scenes:["assets/yell_out.dae"],
  sky:"assets/yell_out_sky.jpg",
  description: "Your voice echoes out into the unseen.  Eyes peering over the thick wall of gray as you feel almost as if suspended in a great space without border. This did not seem right. How was it so easy to lose direction? With a panicked inhale, you look left and right. A moment of silence before a sound returned reply.  Booming, low, and guttural it came from the distance.  It heard you.",
  actions:[
    {
      trigger: "TRIGGER_COVER",
      description: "Cover your ears!",
      page: "cover",
    },
    {
      trigger: "TRIGGER_LISTEN",
      description: "Listen to voice!",
      page: "listen",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([3,4,6,7,8,9]),
})

addPage("listen",{
  scenes:["assets/listen.dae"],
  textBoxOffset: { x:0, y:0.45, z: -0.2 },
  sky:"assets/yell_out_sky.jpg",
  description: "You listen.  There was nothing malicious or evil about it, just so wholly alien to your mind leaves no room for your own psyche.  Long after they found you and put you in a padded room, you continued to feel it’s rumblings, each syllable in time with the beat of your heart.",
  sounds: SceneMusicDTO.makeMusicConfig([6,10,18]),
})

addPage("turn_back",{
  scenes:["assets/turn_back.dae"],
  sky:"assets/house_sky.jpg",
  textBoxOffset: { x:0, y:0.4, z: -0 },
  description: "You attempt to retrace your steps but no matter which way you turn endless mist awaits you.  Your limbs grow weary, hunger and thirst threaten to drive you mad, time loses all meaning.  You spend an eternity wandering the mist, lost forever in time and space.",
  actions:[

  ],
  sounds: SceneMusicDTO.makeMusicConfig([6,7,8]),
})

addPage("cover",{
  scenes:["assets/cover.dae"],
  sky:"assets/house_sky.jpg",
  description: "Though you cannot make out the words, you can feel the sound move through you like a physical force.  It is several minutes after the noise stops before you realize the only sound remaining is your own screaming.  You force your eyes open, and discover the mist has dissipated.",
  actions:[
    {
      trigger: "TRIGGER_HIGHER",
      description: "Seek higher ground",
      page: "higher",
    },
    {
      trigger: "TRIGGER_SHELTER",
      description: "Seek shelter",
      page: "shelter",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([6,8,10]),
})

addPage("higher",{
  scenes:["assets/higher.dae"],
  sky:"assets/house_sky.jpg",
  description: "Still lost, you decide to get your bearings by following the gentle slope of the terrain upwards.  As you crest the nearest hill you realize you are only a few kilometers from Anaya Acres, the farm of an old family friend.",
  actions:[
    {
      trigger: "TRIGGER_ANAYA",
      description: "Travel to Anaya Acres",
      page: "anaya",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([11]),
})

addPage("anaya",{
  scenes:["assets/anaya.dae"],
  sky:"assets/house_sky.jpg",
  description: "Long before you arrive at Anaya Acres the smell of warm bread and home cooked food guides you like the beacon of a lighthouse.  Despite the insanity of the day’s events, you know just a few moments with good food and even better company will make them all seem as distant as a childhood nightmare.",
  actions:[
  ],
  sounds: SceneMusicDTO.makeMusicConfig([11,12,13,14]),
})

addPage("shelter",{
  scenes:["assets/shelter.dae"],
  sky:"assets/house_sky.jpg",
  textBoxOffset: { x:0, y:0.4, z: -0 },
  description: "Weary and frightened, you seek shelter under the boughs of a nearby tree. As you collapse beneath the bows, rattled and terrified as you are you fall into a fitful slumber.",
  actions:[
    {
      trigger: "TRIGGER_DREAM",
      description: "Dream",
      textBoxOffset: { x:0, y:0.45, z: -0 },
      page: "dream",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([11]),
})

addPage("dream",{
  scenes:["assets/dream.dae"],
  sky:"assets/yell_out_sky.jpg",
  description: "You dream you are on a terrifying and alien coast.  In the distance you see oddly shaped cyclopean ruins, it’s architecture redolent of spheres and dimensions apart from ours.  You are drawn to that corpse city, long since dead but yet alive still with some unintelligible presence.  As you walk towards it, you realize you do not now dream, but instead have finally awoken: your consciousness trapped forever in this monstrous wasteland.",
  actions:[
  ],
  sounds: SceneMusicDTO.makeMusicConfig([3,5,6,11]),
})

addPage("follow_them",{
  scenes:["assets/follow_them.dae"],
  sky:"assets/house_sky.jpg",
  description: "Rushing into the woods after them, a concussion knocks you to the ground. After picking yourself up you slowly approach a circle of devastation. Dogs and bodies are lying on the floor. Trees have been uprooted. In the centre of it all kneels a hooded figure.",
  actions:[
    {
      trigger: "TRIGGER_STAY",
      description: "Stay",
      page: "stay",
    },
    {
      trigger: "TRIGGER_FLEE",
      description: "Get out of here!",
      page: "flee",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([2,6,8,16]),
})

addPage("stay",{
  scenes:["assets/stay.dae"],
  sky:"assets/black.jpg",
  description: "Stunned by the blast you find yourself standing still as the hooded figure approaches you.  You barely make a sound as the hooded figure slowly walks to you and slips a curved dagger between your ribs.  He whispers into your ear as your vision darkens: \"From his house at R'lyeh he is no longer dreaming.\"",
  actions:[
    {
      trigger: "TRIGGER_RADIO",
      description: "Listen to radio",
      showText: "You turn up the volume on the radio and listen to the screams and shouts of several men and women overlapping.  Besides the blood curdling screams for help, you make out snippets of what seems to be a recording looping the same report over and over. \"...antarctic project ... beneath the ice ... what have we ...\" ",
    },
    {
      trigger: "TRIGGER_FLEE",
      description: "Get out of here!",
      page: "keep_running",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([8]),
})


addPage("flee",{
  scenes:["assets/flee.dae"],
  sky:"assets/house_sky.jpg",
  description: "You turn and run as fast as your legs can carry you.  You make it several yards before stumbling on a small hand held short wave radio.",
  actions:[
    {
      trigger: "TRIGGER_RADIO",
      description: "Listen to radio",
      showText: "You turn up the volume on the radio and listen to the screams and shouts of several men and women overlapping.  Besides the blood curdling screams for help, you make out snippets of what seems to be a recording looping the same report over and over. \"...antarctic project ... beneath the ice ... what have we ...\" ",
    },
    {
      trigger: "TRIGGER_FLEE",
      description: "Get out of here!",
      page: "keep_running",
    }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([8]),
})

addPage("keep_running",{
  scenes:["assets/approach_the_meteor.dae"],
  sky:"assets/house_sky.jpg",
  textBoxOffset: { x:0, y:0.35, z: 0.25 },
  description: "Discretion being the better part of valor, you decide to get the hell out of here, but as you run you notice a strange fog begin to rise from the ground.  You quickly lose your bearings.",
  actions:[
     {
       trigger: "TRIGGER_RUN",
       description: "Run forward",
       page: "run_forward",
     },
     {
       trigger: "TRIGGER_TURN_BACK",
       description: "Turn back",
       textBoxOffset: { x:0, y:0.4, z: -0.2 },
       page: "turn_back",
     },
     {
       trigger: "TRIGGER_YELL",
       description: "Yell out loudly into the mist",
       page: "yell_out",
     }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([3,6,7,8]),
})

addPage("climb_hill",{
  scenes:["assets/climb-the-hill.dae"],
  sky:"assets/house_sky.jpg",
  textBoxOffset: { x:0, y:0.5, z: 0.0 },
  description: "As you begin climbing the hill you hear the sound of engines approaching rapidly from the distance. Startled, you crouch down behind a log and watch as three helicopters descend to the same spot where you saw the meteor land.",
  actions:[
      //TBD
      {
        trigger: "TRIGGER_Helicopter",
        hoverSounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Helicopter Engines SFX.wav",
          volume:.3,
          name: "helicopter"
        }],
      },
      {
        trigger: "TRIGGER_farmwindmill",
        description: "Get your camera",
        page: "Get_Your_Camera",
      },
      {
        trigger: "TRIGGER_Meteor",
        description: "Watch from a distance",
        page: "Watch_From_Distance",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Meteor SFX 1.wav",
          volume:.6,
          name: "Meteor"
        }]
      }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([1, 5]),
})

addPage("Get_Your_Camera",{
  scenes:["assets/getyourcamera.dae"],
  sky:"assets/house_sky.jpg",
  description: "You run back to your farmhouse to get your camera. There is definitely something bigger going on here.",
  actions:[
      //TBD
      {
        trigger: "TRIGGER_camera",
        description: "Take photos from your front yard",
        page: "TakePhotosFromFrontYard",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Pick Up Camera SFX.wav",
          volume:.3,
          name: "pickupCamera"
        }],
      },
      {
        trigger: "TRIGGER_Door",
        description: "Take photos from the roof",
        page: "TakePhotosFromTheRoof",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Door Transition SFX.wav",
          volume:.3,
          name: "doorTransition"
        }],
      },
      {
        trigger: "TRIGGER_radio",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Radio SFX 2.wav",
          volume:.3,
          name: "radio2"
        }]
      },
      {
        trigger: "TRIGGER_paper",
        showText: "The end is near. I hear a noise at the door, as of some immense slippery body lumbering against it. It shall not find me. God, that hand! The window! The window!",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Paper SFX.wav",
          volume:.3,
          name: "paper"
        }]
      },
      {
        trigger: "TRIGGER_board",
        showText: "Then suddenly I saw it. With only a slight churning to mark its rise to the surface, the thing slid into view above the dark waters. Vast, Polyphemus-like, and loathsome, it darted like a stupendous monster of nightmares to the monolith, about which it flung its gigantic scaly arms, the while it bowed its hideous head and gave vent to certain measured sounds.",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Paper SFX.wav",
          volume:.3,
          name: "board"
        }]
      }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([2,5,8,15]),
})


addPage("TakePhotosFromTheRoof",{
  scenes:["assets/take-photos-from-the-roof.dae"],
  sky:"assets/house_sky.jpg",
  description: "The wind whips around you as you climb to the highest point on the rooftop. As you adjust the focus on the lens you feel a tingle on your spine.  Something grips you around the ankle and pulls your feet out from under you!",
  actions:[
      //TBD
      {
        trigger: "TRIGGER_camera",
        description: "Take a Photo",
        page: "TakeAPhoto",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Camera Shot 1 SFX.wav",
          volume:.3,
          name: "cameraShot"
        }],
      },
      {
        trigger: "TRIGGER_house",
        description: "Grab the chimney",
        page: "GrabChimney",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Door Transition SFX.wav",
          volume:.3,
          name: "doorTransition"
        }],
      }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([8,15,16]),
})

addPage("GrabChimney",{
  scenes:["assets/take-photos-from-the-roof.dae"],
  sky:"assets/house_sky.jpg",
  description: "You desperately try to hold on as you are pulled across the rooftop. You manage to grab the chimney as you turn around to see an alien like tentacle wrapped around your leg. You hear some kind of wet mass pulling itself up as more tentacles appear. Never one to miss a photo opportunity, you get your lens into focus.",
  actions:[
      {
        trigger: "TRIGGER_camera",
        description: "Take a Photo",
        page: "TakeAPhoto",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Camera Shot 1 SFX.wav",
          volume:.3,
          name: "cameraShot"
        }],
      }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([3,7,8,16]),
})

addPage("TakePhotosFromFrontYard",{
  scenes:["assets/take-photos-from-the-front-yard.dae"],
  sky:"",
  description: "You attempt to take a photo of the action but cannot see anything useful from this angle.",
  actions:[
      {
        trigger: "TRIGGER_house",
        description: "Take photos from the roof",
        page: "TakePhotosFromTheRoof",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Door Transition SFX.wav",
          volume:.3,
          name: "doorTransition"
        }],
      }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([2,8,15]),
})
addPage("TakeAPhoto",{
  scenes:["assets/photo.dae"],
  sky:"assets/black.png",
  description: "Years later, upon discovering the photo, a prominent American author would write: “A pulpy, tentacled head surmounted a grotesque and scaly body with rudimentary wings... It represented a monster of vaguely anthropoid outline, but with an octopus-like head whose face was a mass of feelers, a scaly, rubbery-looking body, prodigious claws on hind and fore feet, and long, narrow wings behind.”",
  actions:[
  ],
  sounds: SceneMusicDTO.makeMusicConfig([1,15]),
})

addPage("Watch_From_Distance",{
  scenes:["assets/watchfromadistance.dae"],
  sky:"assets/house_sky.jpg",
  textBoxOffset: { x: 0, y: 0.5, z: 0 },
  description: "You see from the distance person trying to hide in the bushes, and after a while running to nearest wood.  You hear the dogs barking like they have found something. Someone blow the whistle. The other start yelling \"There he is!!!\"",
  actions:[
      //TBD
      {
        trigger: "TRIGGER_Meteor",
        description: "Rush towards the meteor",
        page: "follow_them",
        sounds: [{
          type: "effect",
          loop: false,
          path: "assets/sounds/Jogging Away SFX.wav",
          volume:.3,
          name: "joggingAway"
        }]
      }
  ],
  sounds: SceneMusicDTO.makeMusicConfig([2,5,17]),
})


//
// HOWARD ADDING SCENES PREVENT MERGE CONFLICT DONT EDIT BELOW THIS COMMENT
//
