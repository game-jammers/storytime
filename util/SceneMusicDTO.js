SceneMusicDTO = {};
SceneMusicDTO.sceneToMusicMap = {
    climb_hill: [{
            name:"guitar",
            volume: 1,
            type: "music"
        },
        {
            name: "trashedPiano",
            sound: escape("assets/sounds/stems/Stem #5 (Trashed Piano).wave"),
            volume: 1,
            type: "music"
        },
        {
            name: "bulk",
            volume: 0,
            type: "music",
            soundNames: ["piano", "haunting", "uneasy", "darkWind", "torturedStrings", "kick"]
        }],

        approach_the_meteor: [
        {
            name: "piano",
            volume: 1,
            type: "music"
        },
        {
            name: "haunting",
            volume: 1,
            type: "music"
        },
        {
            name: "bulk",
            volume: 0,
            type: "music",
            soundNames: ["guitar", "uneasy", "trashedPiano", "darkWind", "torturedStrings", "kick"]
        }
    ]
};

const listOfStems = ["guitar", "piano", "haunting", "uneasy", "trashedPiano", "darkWind", "torturedStrings", "kick", "playfulDemon", "bellow", "cello", "bass", "viola", "violin", "ambientGuitar", "stacatto", "fullCelli", "demonBeast"];
SceneMusicDTO.allMusic =  [];
listOfStems.forEach((stemName) => {
  let stemConfig = {
    name: stemName,
    volume: 0,
    type: "music"
  };

  SceneMusicDTO.allMusic.push(stemConfig);
});

SceneMusicDTO.makeMusicConfig = function( indices ) {
    var result = [];

    for( var i = 0; i < SceneMusicDTO.allMusic.length; ++i ) {
        var name = SceneMusicDTO.allMusic[i].name;
        if( indices.includes(i + 1) ) {
            result.push({
                name: name,
                volume: 1,
                loop: true,
                type: "music"
            });
        }
        else {
            //bulk.soundNames.push(name);
            result.push({
                name: name,
                volume: 0,
                loop: true,
                type: "music"
            });
        }
    }

    return result;
}
