class HowlerFacade {

	constructor() {
		this.initStems();
		this.initEffects();
		this.fadeTime = 700;// eventually maybe make this something as part of the configuration

		this.nameToSoundIds = {};
		this.soundIdsToName = {};

		// hack - other sounds that we might want to add quickly
		this.soundIdsToSounds = {};
		this.nameToSoundIdsForQuickAddSounds = {};

		this.currentStemVolumes = {};
		this.currentDialogPlayingSound = undefined; // don't need to set but who cares
		this.currentDialogPlayingSoundId = undefined;

		Object.keys(this.stems).forEach(function(key){
			this.currentStemVolumes[key] = this.stems[key].volume();
		}.bind(this));
	}

	initStems(){
		this.stems = {
			guitar: new Howl({
				src: ["assets/sounds/stems/Stem1(Guitar).wav"],
				loop: true,
				volume: 0
			}),
			piano: new Howl({
				src: ["assets/sounds/stems/Stem2(Piano).wav"],
				loop: true,
				volume: 0
			}),
			haunting: new Howl({
				src: [escape("assets/sounds/stems/Stem3(Haunting ambience).wav")],
				loop: true,
				volume: 0
			}),
			uneasy: new Howl({
				src:  [escape("assets/sounds/stems/Stem #4 (Uneasy).wav")],
				loop: true,
				volume: 0
			}),
			trashedPiano: new Howl({
				src: [escape("assets/sounds/stems/Stem #5 (Trashed Piano).wav")],
				loop: true,
				volume: 0
			}),
			darkWind: new Howl({
				src: [escape("assets/sounds/stems/Stem #6 (Dark Wind).wav")],
				loop: true,
				volume: 0
			}),
			torturedStrings: new Howl({
				src: [escape("assets/sounds/stems/Stem #7 (Tortured Strings).wav")],
				loop: true,
				volume: 0
			}),
			kick: new Howl({
				src: [escape("assets/sounds/stems/Stem8(Kick).wav")],
				loop: true,
				volume: 0
			}),
			playfulDemon: new Howl({
				src: [escape("assets/sounds/stems/Stem #9 (Playful Demon).wav")],
				loop: true,
				volume: 0
			}),
			bellow: new Howl({
				src: [escape("assets/sounds/stems/Stem #10 (Bellow).wav")],
				loop: true,
				volume: 0
			}),
			cello: new Howl({
				src: [escape("assets/sounds/stems/Stem #11 (Cello).wav")],
				loop: true,
				volume: 0
			}),
			bass: new Howl({
				src: [escape("assets/sounds/stems/Stem #12 (Bass).wav")],
				loop: true,
				volume: 0
			}),
			viola: new Howl({
				src: [escape("assets/sounds/stems/Stem #13 (Viola).wav")],
				loop: true,
				volume: 0
			}),
			violin: new Howl({
				src: [escape("assets/sounds/stems/Stem #14 (Violin).wav")],
				loop: true,
				volume: 0
			}),
			ambientGuitar: new Howl({
				src: [escape("assets/sounds/stems/Stem #15 (Ambient Guitar).wav")],
				loop: true,
				volume: 0
			}),
			stacatto: new Howl({
				src: [escape("assets/sounds/stems/Stem #16 Stacatto.wav")],
				loop: true,
				volume: 0
			}),
			fullCelli: new Howl({
				src: [escape("assets/sounds/stems/Stem #17 Full celli.wav")],
				loop: true,
				volume: 0
			}),
			demonBeast: new Howl({
				src: [escape("assets/sounds/stems/Stem #18 (Demon beast).wav")],
				loop: true,
				volume: 0
			}),
		};
	}

	initEffects() {
		this.effects = {
			radio: new Howl({
				src: ['assets/sounds/Radio SFX 4.wav'],
				loop: false,
				onend: function(id){
					this.soundStoppedPlaying(id);
				}.bind(this)
			})
		};
	}

	soundAlreadyPlaying(sound){
		return this.nameToSoundIds[sound.name] !== undefined;
	}

	soundStoppedPlaying(id){
		if(this.currentDialogPlayingSoundId === id){
			this.currentDialogPlayingSoundId = undefined;
			this.currentDialogPlayingSound = undefined;
			return;
		}

		this.removeSound(id);
	}

	playSoundEffect(sound) {
		// hickity hack
		if(sound.path !== undefined){
			if(this.nameToSoundIdsForQuickAddSounds[sound.name] !== undefined){
				return;
			}

			let howlSound = new Howl({
				src: [sound.path],
				loop: sound.loop,
				volume: sound.volume,
				onend: function(id){
					this.soundStoppedPlaying(id);
				}.bind(this)
			});
			// really?
			howlSound.customName = sound.name;

			const id = howlSound.play();
			this.soundIdsToSounds[id] = howlSound;
			this.nameToSoundIdsForQuickAddSounds[sound.name] = id;
			return;
		}

		if(this.soundAlreadyPlaying(sound)){
			return;
		}

		const howlSound = this.effects[sound.name];
		if(howlSound.volume()) {
			howlSound.volume(howlSound);
		}

		return howlSound.play();
	}

	playMusic(sound) {
		if(sound.name === "bulk"){
			this.doBulkOperation(sound);
			return;
		}

		const howlMusic = this.stems[sound.name];
		const currentStemVolume = this.stems[sound.name].volume();
		this.currentStemVolumes[sound.name] = currentStemVolume;

		if(!this.soundAlreadyPlaying(sound)){
			let soundId = howlMusic.play();
			howlMusic.volume(sound.volume);
			this.addSound(sound, soundId);
			return soundId;
		}

		howlMusic.fade(currentStemVolume, sound.volume, this.fadeTime);
	}

	playSound(sound) {
		let soundId;

		switch(sound.type){
			case "effect":
				soundId = this.playSoundEffect(sound);
				break;
			case "dialog":
				soundId = this.playDialog(sound);
				break;
			case "music":
				soundId = this.playMusic(sound);
				break;
			default: console.error("woops, that sounds type is not supported atm...");
		}


		if(soundId !== undefined) {
			this.addSound(sound, soundId);
		}

		return soundId;
	}

	playDialog(sound){
		if(this.currentDialogPlayingSoundId !== undefined){
			this.currentDialogPlayingSound.stop(this.currentDialogPlayingSoundId);
			this.currentDialogPlayingSoundId = undefined;
			this.currentDialogPlayingSound = undefined;
		}
		// I know, dry!! :)
		let howlSound = new Howl({
			src: [sound.path],
			loop: false,
			volume: sound.volume,
			onend: function(id){
				this.soundStoppedPlaying(id);
			}.bind(this)
		});

		this.currentDialogPlayingSoundId = howlSound.play();
		this.currentDialogPlayingSound = howlSound;
	}

	addSound(sound, soundId){
		this.nameToSoundIds[sound.name] = soundId;
		this.soundIdsToName[soundId] = sound.name;
	}

	removeSound(id){
		// another hack
		if(this.soundIdsToSounds[id] !== undefined){
			let growlSound = this.soundIdsToSounds[id];

			this.nameToSoundIdsForQuickAddSounds[growlSound.customName] = undefined;
			this.soundIdsToSounds[id] = undefined;

			delete this.soundIdsToSounds[id];
			delete this.nameToSoundIdsForQuickAddSounds[growlSound.customName];

			return;
		}

		const soundName = this.soundIdsToName[id];
		this.soundIdsToName[id] = undefined;
		delete this.soundIdsToName[id];

		this.nameToSoundIds[soundName] = undefined;
		delete this.nameToSoundIds[soundName];
	}

	doBulkOperation(sound){
		sound.soundNames.forEach(function(soundName){
			// dry!!!
			const howlMusic = this.stems[soundName];
			const currentStemVolume = this.stems[soundName].volume();
			this.currentStemVolumes[soundName] = currentStemVolume;
			howlMusic.fade(currentStemVolume, sound.volume, this.fadeTime);
		}.bind(this));
	}
}
